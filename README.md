# First-stage UEFI Bootloader for NetifyOS

## Description
Initial UEFI bootloader that handles chaining to a trusted full bootloader under secure boot environments. This package contains the version signed by the UEFI signing service.

## Changes for NetifyOS
Some minor branding changes are required.  In addition, the default file permissions were made more restrictive.

## Maintenance
To pull in an upstream update, follow these instructions:

  * git clone git@gitlab.com:netify.ai/netifyos/shim-signed.git
  * cd shim-signed
  * git checkout c7
  * git remote add upstream git://git.centos.org/rpms/shim-signed.git
  * git pull upstream c7
  * git checkout netifyos7
  * git merge --no-commit c7
  * git commit
